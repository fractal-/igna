title = "IGNA"
frame = 0
fillpatterns = {▒,▤}

function initgame()
    gamesystem = {
        gamemode = 0, -- 0 = menu, 1 = tactical, 2 = micro, 3 = gameover, 4 = victory cin
        difficulty = 0,
        turn = 0 -- 0 = player turn, 1 = AI turn
    }

    initmenu()
end

function initmenu()
    music(-1, 300)
    sfx(7)
    menu = {
        textindex = 1,
        texts = {
            'press ☉',
            'a SOLAR BLAST',
            'IS ABOUT TO HIT',
            'OUR PLANET, IGNA.',
            'tAKE THE ROVER AND',
            'RESCUE OUR',
            'FELLOW HUMANS.'
        }
    }
end

function inittacti()
    sfx(-1)
    music(-1, 300)
    music(0)
    playervehicle = {
        x = 7*8,
        y = 4*8,
        sprv = 3,
        sprh = 4,
        dir = 1,
        movepath = {},
        actioncount = 0,
        movecount = 0,
        range = 0,
        ischarging = false,
        ismoving = false,
        enginemodule = {
            integrity = 3,
            charge = 1
        },
        powermodule = {
            integrity = 3,
            charge = 2,
            maxcharge = 3
        },
        bombs = 3
    }

    gameoverinfo = {
        deathinstigator = '',
        steps = 0,
        peoplesaved = 0
    }

    function computerange(vehicle)
        vehicle.range = (vehicle.powermodule.charge == 0) and 0 or (ceil((vehicle.enginemodule.integrity/3)*vehicle.enginemodule.charge*4)) 
        if vehicle.powermodule.charge == 0 then gameoverinfo.deathinstigator = 'not enought power'
        elseif vehicle.range <= 0 then gameoverinfo.deathinstigator = 'could not move' end
    end

    mapobjects = {
        engineparts = {
            {1, 6},
            {10, 46},
            {40, 20}
        },
        powercells = {
            {36, 08}
        },
        people = {
            {
                spr = 6,
                saved = false,
                worldlocation = {21, 25},
                seat = {10, 5},
                rocketseat = {5*8, 9*8},
                dialogue = "anna: WE SHOULD LOOK FOR THE \n OTHERS AND LEAVE WITH\n THE ROCKET!"
            },
            {
                spr = 9,
                saved = false,
                worldlocation = {48, 35},
                seat = {10, 9},
                rocketseat = {7*8, 6*8},
                dialogue = "pierre: MAKE SURE TO FULLY \n charge THE VEHICLE, ELSE WE\n CANNOT MOVE"
            },
            {
                spr = 7,
                saved = false,
                worldlocation = {36, 06},
                seat = {4, 5},
                rocketseat = {10*8, 9*8},
                dialogue = "babouchot: THE VEHICLE HAS\n A landmine DISPENSER\n PRESS ❎ TO DROP ONE ON\n THE GROUND WHEN DRIVING"
            },
            {
                spr = 10,
                saved = false,
                worldlocation = {09, 45},
                seat = {4, 9},
                rocketseat = {8*8, 6*8},
                dialogue = "claire: damage REDUCES THE \n VEHICLE'S speed, FIX IT HERE.\n FIND engine parts TO\n INCREASE THE speed"
            }
        },
        armedbombs = {},
        worms = {
            {21,11, false},
            {3, 20, false},
            {30, 44, false},
            {33, 51, false},
            {38, 18, false}
        }
    }

    pathobject = {
        path = {},
        reachable = {}
    }

    playercursor = {
        x = 7*8,
        y = 4*8,
        spr = 2,
        mode = 0, -- 0 = selection, 1 = move, 2 = action
        hinttext = ''
    }

    playercamera = {
        x = 0,
        y = 0
    }

    fireray = {
        step = -9,
        diagonal = {},
        firelanes = {}
    }

    updatevehicleaction(playervehicle)
    updatereachablesplayer()
    if isplayervehicleonscreen() and not playervehicle.ismoving then updatepathplayer() end
end

function initmicro()

    playerpilot = {
        x = 11*8,
        y = 7*8,
        dir = 2,
        spr = 5,
        sprh = 11,
        spru = 8,
        sprd = 5,
        indialogue = false,
        dialogue = ''
    }

end

function updategame()
    if gamesystem.gamemode == 0 then
        updatemenu()
    elseif gamesystem.gamemode == 1 then
        updatetacti()
    elseif gamesystem.gamemode == 2 then
        updatemicro()
    elseif gamesystem.gamemode == 3 then
        updategameover()
    elseif gamesystem.gamemode == 4 then
        updatevictory()
    end
end

function updatemenu()
    if (btnp(4)) then
        if (menu.textindex < #menu.texts) then
            menu.textindex += 1
        else
            gamesystem.gamemode = 1
            inittacti()
            initmicro()
        end
    end
end

function updatetacti()
    -- CURSOR --
    -- cursor movement inputs
    if (btnp(0)) then
        playercursor.x = max(playercursor.x - 8, 0)
        if isplayervehicleonscreen() and not playervehicle.ismoving then updatepathplayer()
        else
            pathobject.path = {}
        end
    elseif (btnp(1)) then
        playercursor.x = min(playercursor.x + 8, 63*8)
        if isplayervehicleonscreen() and not playervehicle.ismoving then updatepathplayer()
        else
            pathobject.path = {}
        end
    elseif (btnp(2)) then
        playercursor.y = max(playercursor.y - 8, 0)
        if isplayervehicleonscreen() and not playervehicle.ismoving then updatepathplayer()
        else
            pathobject.path = {}
        end
    elseif (btnp(3)) then
        playercursor.y = min(playercursor.y + 8, 63*8)
        if isplayervehicleonscreen() and not playervehicle.ismoving then updatepathplayer()
        else
            pathobject.path = {}
        end
    end
    -- cursor movement limits
    if playercursor.x < playercamera.x + 20 then
        playercamera.x = max (playercamera.x - 2, 0)
    end
    if playercursor.x > playercamera.x + 100 then
        playercamera.x = min(playercamera.x + 2, 63*8 - 120)
    end
    if playercursor.y < playercamera.y + 20 then
        playercamera.y = max (playercamera.y - 2, 0)
    end
    if playercursor.y > playercamera.y + 100 then
        playercamera.y = min(playercamera.y + 2, 63*8 - 120)
    end

    -- cursor hovering hint text
    if (getcursortile() == 78) then 
        playercursor.hinttext = 'power station'
    elseif (getcursortile() == 92) then 
        playercursor.hinttext = 'fellow human'
    else
        playercursor.hinttext = ''
        for _,engpart in pairs(mapobjects.engineparts) do
            if (playercursor.x == gridtopixel(engpart[1]) and playercursor.y == gridtopixel(engpart[2])) then
                playercursor.hinttext = 'engine part'
            end
        end
        for _,powercell in pairs(mapobjects.powercells) do
            if (playercursor.x == gridtopixel(powercell[1]) and playercursor.y == gridtopixel(powercell[2])) then
                playercursor.hinttext = 'power cell'
            end
        end
        for _,worm in pairs(mapobjects.worms) do
            if (playercursor.x == gridtopixel(worm[1]) and playercursor.y == gridtopixel(worm[2])) then
                playercursor.hinttext = 'sands worm'
            end
        end
    end

    -- cursor hovering style
    if (iscursoronplayer()) then
        playercursor.spr = 1
        playercursor.hinttext = '☉ to enter'
    else
        playercursor.spr = 2
    end


    -- cursor action
    if (btnp(4)) then
        if iscursoronplayer() then
            gamesystem.gamemode = 2
        elseif (not playervehicle.ismoving) and count(pathobject.path) > 0 then
            initmovevehicle(playervehicle, pathobject.path)
        end
    end

    -- drop bomb
    if (btnp(5) and not playervehicle.ismoving) then
        local anybombhere = false
        for _,i in pairs(mapobjects.armedbombs) do
            if (i[1] == playervehicle.x and i[2] == playervehicle.y) then
                anybombhere = true
            end
        end
        if (not anybombhere) then
            add(mapobjects.armedbombs, {playervehicle.x, playervehicle.y})
            sfx(5)
        end
    end

    updatesunrays(fireray.step)

    updatevehiclelocation(playervehicle)

end

function updatepathplayer()
    pathobject.path  = pathfind(pixeltogrid(playervehicle.x), pixeltogrid(playervehicle.y), pixeltogrid(playercursor.x), pixeltogrid(playercursor.y))
end

function updatereachablesplayer()
    pathobject.reachable = getreachables(pixeltogrid(playervehicle.x), pixeltogrid(playervehicle.y), playervehicle.range)
end

function updatevehiclelocation(vehicle)
    if vehicle.ismoving and playervehicle.range > 0 then
        local currentcase = vehicle.movepath[1]
        local targetcase = vehicle.movepath[2]
        if targetcase == nil then
            vehicle.ismoving = false 
            updatereachablesplayer()
            updatepathplayer()
            return
        end
        local dir = {gridtopixel(targetcase[1]) - vehicle.x, gridtopixel(targetcase[2]) - vehicle.y}
        if (dir[1] == 0 and dir[2] == 0) then
            del(vehicle.movepath, vehicle.movepath[1])
            vehicle.actioncount += 1
            vehicle.movecount += 1
            updatevehicleaction(vehicle)
        end   
        if dir[1] > 0 then vehicle.dir = 3 end 
        if dir[1] < 0 then vehicle.dir = 2 end 
        if dir[2] > 0 then vehicle.dir = 1 end 
        if dir[2] < 0 then vehicle.dir = 0 end
        vehicle.x += (dir[1] > 0) and 1 or ((dir[1] < 0) and -1 or 0) 
        vehicle.y += (dir[2] > 0) and 1 or ((dir[2] < 0) and -1 or 0) 
        sfx(0)
    end
end

function updatesunrays(step)
    for substep = 1, 4 do
        fireray.firelanes[substep] = {}
        for _,i in pairs(gettable2diagonal(64, 64, step + (substep-1))) do
            fireray.firelanes[substep][i] = {i[1], i[2]}
        end
    end
    if playervehicle.range == 0 then
        fireray.step += 1
    end
    if step > 64 then 
        -- gameoverinfo.deathinstigator = 'burnt by the sun'
        gameover() 
    end
end

function initmovevehicle(vehicle, path)
    vehicle.movepath[1] = {vehicle.x, vehicle.y}
    for i = 1, min(count(path), vehicle.range) do
        vehicle.movepath[i+1] = path[i]
    end
    vehicle.ismoving = true
end

function updatevehicleaction(vehicle)

    local currenttile = mget(pixeltogrid(playervehicle.x), pixeltogrid(playervehicle.y))

    --victory
    if (vehicle.x == gridtopixel(59) and vehicle.y == gridtopixel(60)) then
        victory()
    end
    --printh('debug: actioncount='..vehicle.actioncount, 'actioncount.txt')
    --printh('debug: range='..vehicle.range, 'range.txt')
    
    -- fire ray getting closer
    if vehicle.actioncount % vehicle.range == 0 then 
        fireray.step += 1
    end

    -- worms update
    if vehicle.actioncount % flr(vehicle.range / 4) == 0 then 
        updateworms()
    end

    if (fget(currenttile, 7)) then
        vehiclehit(vehicle)
    end

    if (fget(currenttile, 6)) then
        gameoverinfo.deathinstigator = 'burnt by the sun'
        gameover()
    end

    --power consumed when moving
    if (vehicle.movecount != 0 and vehicle.movecount % 32 == 0) then 
        vehicle.powermodule.charge -= 1
        sfx(6)
    end
    
    -- engine part found
    for _,engpart in pairs(mapobjects.engineparts) do
        if (vehicle.x == gridtopixel(engpart[1]) and vehicle.y == gridtopixel(engpart[2])) then
            del(mapobjects.engineparts, engpart)
            vehicle.enginemodule.charge += 1
            sfx(3)
        end
    end

    -- powercell found
    for _,powercell in pairs(mapobjects.powercells) do
        if (vehicle.x == gridtopixel(powercell[1]) and vehicle.y == gridtopixel(powercell[2])) then
            del(mapobjects.powercells, powercell)
            vehicle.powermodule.maxcharge += 1
            vehicle.powermodule.charge += 1
            sfx(3)
        end
    end

    --people found
    if (currenttile == 92) then
        for _,i in pairs(mapobjects.people) do
            if (i.saved == false and pixeltogrid(playervehicle.x) == i.worldlocation[1] and pixeltogrid(playervehicle.y) == i.worldlocation[2]) then
                i.saved = true
                mset(pixeltogrid(playervehicle.x), pixeltogrid(playervehicle.y), 65)
                gamesystem.gamemode = 2
                break
            end
        end
    end
    
    --charging station
    if (fget(currenttile, 2) and not vehicle.ischarging) then
        vehicle.ischarging = true
        vehicle.powermodule.charge = vehicle.powermodule.maxcharge
        stopvehicle(vehicle)
        sfx(2)
    else
        vehicle.ischarging = false
    end

    --hit
    --spikes
    if fget(currenttile, 1) then
        vehiclehit(vehicle)
    end
    -- bombs
    for _,i in pairs(mapobjects.armedbombs) do
        if (i[1] == playervehicle.x and i[2] == playervehicle.y) then
            vehiclehit(vehicle)
            del(mapobjects.armedbombs, i)
        end
    end

    computerange(vehicle)
end

function updateworms()
    local wormpath = {}
    for _,worm in pairs(mapobjects.worms) do
        if (fget(mget(worm[1], worm[2]), 6)) then
            del(mapobjects.worms, worm)
        else
            if worm[3] == true then
                wormpath = pathfind(worm[1], worm[2], pixeltogrid(playervehicle.x), pixeltogrid(playervehicle.y))
                if (wormpath[1] != nil) then
                    worm[1] = wormpath[1][1]
                    worm[2] = wormpath[1][2]
                end
                -- bombs
                for _,i in pairs(mapobjects.armedbombs) do
                    if (i[1] == gridtopixel(worm[1]) and i[2] == gridtopixel(worm[2])) then
                        del(mapobjects.worms, worm)
                        del(mapobjects.armedbombs, i)
                        mset(worm[1], worm[2], 12)
                        sfx(4)
                    end
                end
                if (playervehicle.x == gridtopixel(worm[1]) and playervehicle.y == gridtopixel(worm[2])) then
                    vehiclehit(playervehicle)
                    worm[3] = false
                end
                if (abs(playervehicle.x - gridtopixel(worm[1])) > 64 or abs(playervehicle.y - gridtopixel(worm[2])) > 64) then
                    worm[3] = false
                end
            elseif (abs(playervehicle.x - gridtopixel(worm[1])) < 32 and abs(playervehicle.y - gridtopixel(worm[2])) < 32) then
                worm[3] = true
            end
        end
        --printh('updateworms: ' ..(worm[3] and 'true' or 'false') ..'distance X:' ..abs(playervehicle.x - gridtopixel(worm[1])), 'worms.txt')
    end
end

function vehiclehit(vehicle)
    sfx(4)
    gameoverinfo.deathinstigator = 'vehicle destroyed'
    if (rnd(2) > 1) then
        vehicle.enginemodule.integrity = max(0, vehicle.enginemodule.integrity-1)
    else
        vehicle.powermodule.integrity = max(0, vehicle.powermodule.integrity-1)
    end
    vehicle.powermodule.charge = min(vehicle.powermodule.integrity, vehicle.powermodule.charge)  
    computerange(vehicle) 
    stopvehicle(vehicle)
end

function stopvehicle(vehicle)
    vehicle.movecount = 0
    vehicle.ismoving = false
    vehicle.movepath = {}
    updatereachablesplayer()
    updatepathplayer()
end

function updatemicro()

    local pilottile = mget(pixeltogrid(playerpilot.x)+68, pixeltogrid(playerpilot.y)+1)

    if (not playerpilot.indialogue) then
        if btnp(0) then --left
            if canpilotwalkdirection(2) then
                playerpilot.x = max(playerpilot.x - 8, 0)
                --updatepilotaction()
            end
            playerpilot.dir = 2
        elseif btnp(1) then --right
            if canpilotwalkdirection(3) then
                playerpilot.x = min(playerpilot.x + 8, 15*8)
                --updatepilotaction()
            end
            playerpilot.dir = 3
        elseif btnp(2) then --up
            if canpilotwalkdirection(0) then
                playerpilot.y = max(playerpilot.y - 8, 0)
                --updatepilotaction()
            end
            playerpilot.dir = 0
        elseif btnp(3) then --down
            if canpilotwalkdirection(1) then
                playerpilot.y = min(playerpilot.y + 8, 15*8)
                --updatepilotaction()
            end
            playerpilot.dir = 1
        end

        if (btnp(4)) then
            --return to command
            if (mget(pixeltogrid(playerpilot.x)+68, pixeltogrid(playerpilot.y)+1) == 40) then --door
                gamesystem.gamemode = 1
            end
            --printh('pilot tile = ' ..pilottile, 'macrofix.txt')
            --fix power
            if (pilottile == 47 or pilottile == 51) then
                playerpilot.indialogue = true
                playerpilot.dialogue = (playervehicle.powermodule.integrity == 3) and 'power core: ok' or 'fixing power core'
                playervehicle.powermodule.integrity = 3
                --printh('fixing: power', 'macrofix.txt')
            end
            --fix engine
            if (pilottile == 52) then
                playerpilot.indialogue = true
                playerpilot.dialogue = (playervehicle.enginemodule.integrity == 3) and 'engine: ok' or 'fixing engine'
                playervehicle.enginemodule.integrity = 3
                --printh('fixing: engine', 'macrofix.txt')
            end

            -- dialogue with people saved
            for _,i in pairs(mapobjects.people) do
                if (i.saved == true) then
                    if (playerpilot.dir == 0 and (playerpilot.x == gridtopixel(i.seat[1])) and (playerpilot.y == gridtopixel(i.seat[2]+1)) ) then
                        playerpilot.indialogue = true
                        playerpilot.dialogue = i.dialogue
                        --printh('dialogue:'..i.dialogue, 'dialogue.txt')
                    end
                    if (playerpilot.dir == 1 and (playerpilot.x == gridtopixel(i.seat[1])) and (playerpilot.y == gridtopixel(i.seat[2]-1)) ) then
                        playerpilot.indialogue = true
                        playerpilot.dialogue = i.dialogue
                        --printh('dialogue:'..i.dialogue, 'dialogue.txt')
                    end
                end
            end
        end
    else
        if (btnp(4)) then
            playerpilot.indialogue = false
        end
    end
end

function canpilotwalkdirection(dir)
    if (dir == 0) then
        return (not fget(mget(pixeltogrid(playerpilot.x)+68, pixeltogrid(playerpilot.y)+1 - 1), 0))
    end
    if (dir == 1) then
        return (not fget(mget(pixeltogrid(playerpilot.x)+68, pixeltogrid(playerpilot.y)+1 + 1), 0))
    end
    if (dir == 2) then
        return (not fget(mget(pixeltogrid(playerpilot.x)+68 - 1, pixeltogrid(playerpilot.y)+1), 0))
    end
    if (dir == 3) then
        return (not fget(mget(pixeltogrid(playerpilot.x)+68 + 1, pixeltogrid(playerpilot.y)+1), 0))
    end
end

function updatepilotaction()
    
end

function updategameover()
    if (btnp(4) or frame > 80) then
        reload(0x1000, 0x1000, 0x2000)
        gamesystem.gamemode = 0
        initmenu()
    end
end

function updatevictory()
    if (frame > 200) then
        reload(0x1000, 0x1000, 0x2000)
        gamesystem.gamemode = 0
        initmenu()
    end
end

function gameover()
    gamesystem.gamemode = 3
    frame = 0
    music(0)
end

function victory()
    music(-1, 300)
    gamesystem.gamemode = 4
    frame = 0
    sfx(10)
end


function drawgame()
    if gamesystem.gamemode == 0 then
        drawmenu()
    elseif  gamesystem.gamemode == 1 then
        drawtacti()
    elseif gamesystem.gamemode == 2 then
        drawmicro()
    elseif gamesystem.gamemode == 3 then
        drawgameover()
    elseif gamesystem.gamemode == 4 then
        drawvictory()
    end
    frame += 1
end

function drawmenu()
    cls(1)
    --fillp(fillpatterns[1])
    --circfill(63,64, 48, 7)
    fillp()
    circfill(64,64,48,7)
    ovalfill(64-48+2, 64-48, 128-(64-48), 128-(64-48),10)
    ovalfill(64-48+8, 64-48, 128-(64-48), 128-(64-48),9)
    print(menu.texts[menu.textindex],hcenter(menu.texts[menu.textindex]),80, 8)
    print("BY bozarre",hcenter("BY bozarre"),120, 7)
    spr(96, 38, 56, 2, 2)
    spr(98, 51, 56, 2, 2)
    spr(100, 64, 56, 2, 2)
    spr(102, 79, 56, 2, 2)
end

function drawtacti()
    -- draw map
    camera(playercamera.x, playercamera.y)
    map(0,0,0,0,64,64)

    -- engine parts
    for _,engpart in pairs(mapobjects.engineparts) do
        spr(18, gridtopixel(engpart[1]), gridtopixel(engpart[2]))
    end
    -- power cells
    for _,powercell in pairs(mapobjects.powercells) do
        spr(16, gridtopixel(powercell[1]), gridtopixel(powercell[2]))
    end
    
    if isplayervehicleonscreen() and not playervehicle.ismoving and playervehicle.range > 0 then
        for _,point in pairs(pathobject.reachable) do
            rectfill(gridtopixel(point[1]) +3, gridtopixel(point[2])+3, gridtopixel(point[1]) +4, gridtopixel(point[2])+4, 13)
        end

        for i in pairs(pathobject.path) do
            point = pathobject.path[i]
            rectfill(gridtopixel(point[1]) +3, gridtopixel(point[2])+3, gridtopixel(point[1]) +4, gridtopixel(point[2])+4, i > playervehicle.range and 8 or 12)
        end
        if min(playervehicle.range, count(pathobject.path)) > 0 then
            point = pathobject.path[min(playervehicle.range, count(pathobject.path))]
            if point != nil then ovalfill(gridtopixel(point[1]) +3, gridtopixel(point[2])+3, gridtopixel(point[1]) +4, gridtopixel(point[2])+4, 7) end
        end
    end

    spr((playervehicle.dir < 2) and playervehicle.sprv or playervehicle.sprh, playervehicle.x, playervehicle.y, 1, 1, (playervehicle.dir == 3) and true or false, (playervehicle.dir == 1) and true or false)

    -- bombs
    for _,i in pairs(mapobjects.armedbombs) do
        spr(13, i[1], i[2])
    end

    --worms
    for _,worm in pairs(mapobjects.worms) do
        if worm[3] == true then
            spr(14 + (frame%8)/4, gridtopixel(worm[1]), gridtopixel(worm[2]))
        else
            spr(31, gridtopixel(worm[1]), gridtopixel(worm[2]))
        end
    end

    --power station charging
    if (playervehicle.ischarging) then
        spr(127, playervehicle.x, playervehicle.y, rnd(8)/8, rnd(8)/8)
    end
    
    -- render fire rays
    for substep = 1, 4 do
        for _,i in pairs(fireray.firelanes[substep]) do
            mset(i[1], i[2], 80 + (substep -1))
        end
    end
    
    -- render cursor
    spr(playercursor.spr, playercursor.x, playercursor.y)

    -- draw hud
    camera()

    rectfill(0, 120, 29, 128, 0)

    -- power
    rect(0, 119, 15, 128, 5)
    spr(16 + (frame%4/2), 1, 120)
    local powercharge = playervehicle.powermodule.charge
    print(powercharge, 10, 122, max(8, powercharge + 7))
    
    rectfill(0, 117, 9, 118, 5)
    local powerintegrity = playervehicle.powermodule.integrity
    if powerintegrity > 2 then 
        rectfill(1, 118, 2, 118, 11) 
        rectfill(4, 118, 5, 118, 11) 
        rectfill(7, 118, 8, 118, 11) 
    elseif powerintegrity > 1 then 
        rectfill(1, 118, 2, 118, 10) 
        rectfill(4, 118, 5, 118, 10) 
        rectfill(7, 118, 8, 118, 0) 
    elseif powerintegrity > 0 then 
        rectfill(1, 118, 2, 118, 8) 
        rectfill(4, 118, 5, 118, 0) 
        rectfill(7, 118, 8, 118, 0) 
    else
        rectfill(1, 118, 2, 118, 0) 
        rectfill(4, 118, 5, 118, 0) 
        rectfill(7, 118, 8, 118, 0) 
    end

    -- engine
    rect(15, 119, 30, 128, 5)
    spr(18 + (frame%4/2), 16, 120)
    local enginecharge = playervehicle.enginemodule.charge
    print(enginecharge, 25, 122, max(8, enginecharge + 7))
    
    rectfill(15, 117, 24, 118, 5)
    local engineintegrity = playervehicle.enginemodule.integrity
    if engineintegrity > 2 then 
        rectfill(16, 118, 17, 118, 11) 
        rectfill(19, 118, 20, 118, 11) 
        rectfill(22, 118, 23, 118, 11) 
    elseif engineintegrity > 1 then 
        rectfill(16, 118, 17, 118, 10) 
        rectfill(19, 118, 20, 118, 10) 
        rectfill(22, 118, 23, 118, 0) 
    elseif engineintegrity > 0 then 
        rectfill(16, 118, 17, 118, 8) 
        rectfill(19, 118, 20, 118, 0) 
        rectfill(22, 118, 23, 118, 0) 
    else
        rectfill(16, 118, 17, 118, 0) 
        rectfill(19, 118, 20, 118, 0) 
        rectfill(22, 118, 23, 118, 0) 
    end

    -- hint text
    if (#playercursor.hinttext > 0) then rectfill(48, 119, 51 + (#playercursor.hinttext * 4), 128, 6) end

    print(playercursor.hinttext, 50, 121, 1)
    rectfill(30, 119, 40, 128, 0)
    rect(30, 119, 40, 128, 5)
    print((32 - playervehicle.movecount % 32), 32, 122, (playervehicle.movecount % 32) > 28 and 8 or 7)

    --debug
end

function drawmicro()
    cls(0)
    --camera(68,1)
    map(68,1,0,0,18,18)
    --camera()

    -- engine and power
    spr(16 + (frame%4/2), 7*8, 7*8)
    spr(18 + (frame%4/2), 2*8, 7*8)
    -- integrity fire
    if (playervehicle.enginemodule.integrity < 3) then spr(32 + (frame%6/2), 2*8, 7*8) end
    if (playervehicle.powermodule.integrity < 3) then spr(32 + (frame%6/2), 7*8, 7*8) end

    --player pilot
    playerpilot.spr = (playerpilot.dir == 0) and playerpilot.spru or ((playerpilot.dir == 1) and playerpilot.sprd or playerpilot.sprh)
    spr(playerpilot.spr, playerpilot.x, playerpilot.y, 1, 1, (playerpilot.dir == 3) and true or false, false)
    
    --people saved
    for _,i in pairs(mapobjects.people) do
        if (i.saved == true) then
            spr(i.spr, gridtopixel(i.seat[1]), gridtopixel(i.seat[2]))
        end
    end
    
    -- draw hud
    camera()


    --dialogue window

    if playerpilot.indialogue then
        rectfill(0, 0, 128, 32, 7)
        rect(0, 0, 128, 32, 1)
        print(playerpilot.dialogue, 5, 5, 1)
    end

    rectfill(0, 120, 29, 128, 0)

    -- power
    rect(0, 119, 14, 128, 5)
    spr(16 + (frame%4/2), 0, 120)
    local powercharge = playervehicle.powermodule.charge
    print(powercharge, 9, 122, max(8, powercharge + 7))
    
    rectfill(0, 117, 9, 118, 5)
    local powerintegrity = playervehicle.powermodule.integrity
    if powerintegrity > 2 then 
        rectfill(1, 118, 2, 118, 11) 
        rectfill(4, 118, 5, 118, 11) 
        rectfill(7, 118, 8, 118, 11) 
    elseif powerintegrity > 1 then 
        rectfill(1, 118, 2, 118, 10) 
        rectfill(4, 118, 5, 118, 10) 
        rectfill(7, 118, 8, 118, 0) 
    elseif powerintegrity > 0 then 
        rectfill(1, 118, 2, 118, 8) 
        rectfill(4, 118, 5, 118, 0) 
        rectfill(7, 118, 8, 118, 0) 
    else
        rectfill(1, 118, 2, 118, 0) 
        rectfill(4, 118, 5, 118, 0) 
        rectfill(7, 118, 8, 118, 0) 
    end

    -- engine
    rect(14, 119, 30, 128, 5)
    spr(18 + (frame%4/2), 16, 120)
    local enginecharge = playervehicle.enginemodule.charge
    print(enginecharge, 25, 122, max(8, enginecharge + 7))
    
    rectfill(14, 117, 23, 118, 5)
    local engineintegrity = playervehicle.enginemodule.integrity
    if engineintegrity > 2 then 
        rectfill(15, 118, 16, 118, 11) 
        rectfill(18, 118, 19, 118, 11) 
        rectfill(21, 118, 22, 118, 11) 
    elseif engineintegrity > 1 then 
        rectfill(15, 118, 16, 118, 10) 
        rectfill(18, 118, 19, 118, 10) 
        rectfill(21, 118, 22, 118, 0) 
    elseif engineintegrity > 0 then 
        rectfill(15, 118, 16, 118, 8) 
        rectfill(18, 118, 19, 118, 0) 
        rectfill(21, 118, 22, 118, 0) 
    else
        rectfill(15, 118, 16, 118, 0) 
        rectfill(18, 118, 19, 118, 0) 
        rectfill(21, 118, 22, 118, 0) 
    end
end

function drawgameover()
    cls(1)
    --fillp(fillpatterns[1])
    --circfill(63,64, 48, 7)
    fillp()
    circfill(64,64,max(128-frame, 0),7)
    print("game over",hcenter("game over"),32,8)
    print(gameoverinfo.deathinstigator,hcenter(gameoverinfo.deathinstigator),48,8)
end

function drawvictory()
    cls(0)
    --camera(68,1)
    map(72,26,0,0,18,18)

    spr(005, 56, 71)

    --people saved
    for _,i in pairs(mapobjects.people) do
        if (i.saved == true) then
            spr(i.spr, i.rocketseat[1], i.rocketseat[2])
        end
    end
    camera(rnd(4) - 2, rnd(4) - 2)
end

function iscursoronplayer()
    if (playercursor.x == playervehicle.x and playercursor.y == playervehicle.y) then
        return true
    else 
        return false
    end
end

function getcursortile()
    return mget(pixeltogrid(playercursor.x), pixeltogrid(playercursor.y))
end

function isplayervehicleonscreen()
    return (playervehicle.x >= playercamera.x - 8) and (playervehicle.x <= playercamera.x + 128 + 8) and (playervehicle.y >= playercamera.y - 8) and (playervehicle.y <= playercamera.y + 128 + 8)
end
   

function gridtopixel2(gpoint)
    return {gpoint[1]*8,gpoint[2]*8}
end

function gridtopixel(gi)
    return gi*8
end

function pixeltogrid2(ppoint)
    return {ppoint[1]/8,ppoint[2]/8}
end

function pixeltogrid(pi)
    return pi/8
end

function lerp2(source, target, alpha)
    return {(1 - alpha)*source[1] + alpha*target[1], (1 - alpha)*source[2] + alpha*target[2]}
end

function hcenter(s)
    -- screen center minus the
    -- string length times the 
    -- pixels in a char's width,
    -- cut in half
    return 64-#s*2
  end
  
function vcenter(s)
-- screen center minus the
-- string height in pixels,
-- cut in half
return 61
end

function  gettable2diagonal(sizex, sizey, index)
    local diagonal = {}
    for i=0, sizex do
        for j=0, sizey do
            if flr((i + j)/2) == index then
                add(diagonal, {i, j})
            end
        end
    end
    return diagonal
end

function round(num)
    return flr((num + 4)>>3)<<3
  end
  