
function pathfind(fromx, fromy, tox, toy)
    --a*
    blockerflag = 0
    start = {fromx, fromy}
    goal = {tox, toy}
    path = {}
    searchrange = 32
    reachbox = {fromx - searchrange, fromy - searchrange, fromx + searchrange, fromy + searchrange}

    frontier = {}
    insert(frontier, start, 0)
    came_from = {}
    came_from[vectoindex(start, searchrange)] = nil
    cost_so_far = {}
    cost_so_far[vectoindex(start, searchrange)] = 0

    if fget(mget(tox,toy), blockerflag) or (fromx == tox and fromy == toy) then --  
        return {}
    end
   
    while (#frontier > 0 and #frontier < 512) do
        current = popEnd(frontier)

        -- goal is reached
        if vectoindex(current, searchrange) == vectoindex(goal, searchrange) then
            break 
        end

        local neighbours = getNeighbours(current, reachbox)
        for next in all(neighbours) do
            local nextIndex = vectoindex(next, searchrange)

            local new_cost = cost_so_far[vectoindex(current, searchrange)]  + 1 -- add extra costs here

            if (cost_so_far[nextIndex] == nil) or (new_cost < cost_so_far[nextIndex]) then
                cost_so_far[nextIndex] = new_cost
                local priority = new_cost + heuristic(goal, next)
                came_from[nextIndex] = current
                insert(frontier, next, priority)

            end 
        end
    end
   
    if not fget(mget(tox,toy), 0) then
        add(path, goal)
        current = came_from[vectoindex(goal, searchrange)]
        local cindex = vectoindex(current, searchrange)
        local sindex = vectoindex(start, searchrange)

        while cindex != sindex do
            add(path, current)
            current = came_from[cindex]
            cindex = vectoindex(current, searchrange)
        end
        reverse(path)
    end
      
    return path
end

function getreachables(fromx, fromy, searchrange)
    -- breadth first search
    blockerflag = 0
    start = {fromx, fromy}
    validpoints = {}
    reachbox = {fromx - searchrange, fromy - searchrange, fromx + searchrange, fromy + searchrange}

    frontier = {}
    insert(frontier, start, 0)
    came_from = {}
    came_from[vectoindex(start, searchrange)] = nil
    cost_so_far = {}
    cost_so_far[vectoindex(start, searchrange)] = 0
   
    while (#frontier > 0 and #frontier < 512) do
        current = popEnd(frontier)

        local neighbours = getNeighbours(current, reachbox)
        for next in all(neighbours) do
            local nextIndex = vectoindex(next, searchrange)

            local new_cost = cost_so_far[vectoindex(current, searchrange)]  + 1 -- add extra costs here

            if (cost_so_far[nextIndex] == nil) or (new_cost < cost_so_far[nextIndex]) then
                cost_so_far[nextIndex] = new_cost
                local priority = 0 --new_cost + heuristic(goal, next)
                came_from[nextIndex] = current
                insert(frontier, next, priority)

                if new_cost <= searchrange then
                    add(validpoints, {next[1], next[2]})
                end
            end 
        end
    end
      
    return  validpoints
end


-- manhattan distance on a square grid
function heuristic(a, b)
    return abs(a[1] - b[1]) + abs(a[2] - b[2])
   end
   
-- find all existing neighbours of a position that are not walls
function getNeighbours(pos, reachbox)
    local neighbours={}
    local x = pos[1]
    local y = pos[2]
    if x > reachbox[1] and not fget(mget(x-1,y), blockerflag) then
        add(neighbours,{x-1,y})
    end
    if x < reachbox[3] and not fget(mget(x+1,y), blockerflag) then
        add(neighbours,{x+1,y})
    end
    if y > reachbox[2] and not fget(mget(x,y-1), blockerflag) then
        add(neighbours,{x,y-1})
    end
    if y < reachbox[4] and not fget(mget(x,y+1), blockerflag) then
        add(neighbours,{x,y+1})
    end

    return neighbours
end

-- insert into start of table
function insert(t, val)
    for i=(#t+1),2,-1 do
        t[i] = t[i-1]
    end
    t[1] = val
end

-- insert into table and sort by priority
function insert(t, val, p)
    if #t >= 1 then
        add(t, {})
        for i=(#t),2,-1 do
            local next = t[i-1]
            if p < next[2] then
                t[i] = {val, p}
                return
            else
                t[i] = next
            end
        end
        t[1] = {val, p}
    else
        add(t, {val, p}) 
    end
end

-- pop the last element off a table
function popEnd(t)
    local top = t[#t]
    del(t,t[#t])
    return top[1]
end

function reverse(t)
    for i=1,(#t/2) do
        local temp = t[i]
        local oppindex = #t-(i-1)
        t[i] = t[oppindex]
        t[oppindex] = temp
    end
end

-- translate a 2d x,y coordinate to a 1d index and back again
function vectoindex(vec, searchrange)
    return maptoindex(vec[1],vec[2], searchrange)
end
function maptoindex(x, y, searchrange)
    return ((x+1) * (searchrange * 2 + 1)) + y
end
function indextomap(index, searchrange)
    local x = (index-1)/(searchrange * 2 + 1)
    local y = index - (x*w)
    return {x,y}
end


-- pop the first element off a table (unused)
function pop(t)
    local top = t[1]
    for i=1,(#t) do
        if i == (#t) then
        del(t,t[i])
        else
        t[i] = t[i+1]
        end
    end
    return top
end